package com.pds.CursoSpring;

import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pds.interfaces.IEquipo;

import primercodigo.beans.AppConfig;
import primercodigo.beans.AppConfig2;
import primercodigo.beans.Barcelona;
import primercodigo.beans.Ciudad;
import primercodigo.beans.Jugador;
import primercodigo.beans.Juventus;
import primercodigo.beans.Mundo;
import primercodigo.beans.Persona;

public class App {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Elija un equipo: 1- Barcelona 2- Juventus");
		int respuesta = sc.nextInt();
		
		ApplicationContext appContext = new AnnotationConfigApplicationContext(com.pds.CursoSpring.AppConfig.class);
		Jugador jug =(Jugador)appContext.getBean("jugador1");

		switch (respuesta) {
		case 1:
			jug.setEquipo(new Barcelona());
			break;
			
		case 2:
			jug.setEquipo(new Juventus());
			break;
		default:
			break;
		}
		
						
		System.out.println(jug.getNombre() + "-" + jug.getEquipo().mostrar() + "-" + jug.getCamiseta().getNumero() +"-"+ jug.getCamiseta().getMarca().getNombre());
		((ConfigurableApplicationContext)appContext).close();
	}

}
