package primercodigo.beans;

import org.springframework.stereotype.Component;

import com.pds.interfaces.IEquipo;

@Component
public class Barcelona implements IEquipo{
	
	public String mostrar() {
		return "Barcelona FC";
	}

}
